# tdbg Debugger

## Introduction

The `tdbg` debugger is a powerful terminal-based interface for debugging
applications using the Debug Anything Protocol. It allows developers to
connect to a running application via a WebSocket connection and provides tools
to monitor and control the execution flow of the application.

## Installation

`tdbg` is run from the terminal and does not require installation. Simply
ensure that the executable is in your path or run it from the location where it
is stored.

## Usage

To start debugging with `tdbg`, you will need the WebSocket URL of the running
application and a registration code if required.

```plaintext
Usage: tdbg [options] [wsurl]

Arguments:
  wsurl                           WebSocket URL (default: "ws://127.0.0.1:6400")

Options:
  -r, --registration_code <type>  Registration code to connect to the debugger server
  -n, --name <session-name>       Name for this session
  -l, --log_activity              Log all DAP messages to the debug log
```

If your application requires a registration code, be sure to add it to the command line.

If you set the `TDBG_DEBUG_FILE` prior to launching tdbg, it will log it's internal
activity to the file you provide, this can be helpful when debugging the interaction
with a new service.

## Launching tdbg

When you launch `tdbg`, you will be greeted by the following panes:

- **Code Viewer Area**: Shows the current state of the code being debugged.
- **Watch List**: Located at the top right, this area allows monitoring the values of variables.
- **System Log**: Below the code area, it logs messages from the system being debugged.
- **Command Input Area**: At the bottom, where you input commands to control the debugger.
- **Status Line**: At the very bottom, displaying the current status of the debugger.

## Navigation

- Use standard keyboard navigation for the command input (left/right arrows, `Ctrl+A`, `Ctrl+E`).
- `Up Arrow` and `Down Arrow` to scroll through command history.
- `Tab` to cycle between the different panes.
- In the watch area, use up/down arrows to move between watch variables and `Enter` to open them in the data explorer.
- Press `f` in the data explorer window to toggle between normal and flattened JSON display.

## Understanding Contexts in tdbg

In `tdbg`, a **context** represents a unique execution environment or thread
within your application. It encapsulates the state of the application at a
specific point in time, including variables, the call stack, and the command
that's currently executing. Managing contexts is crucial for debugging complex
applications where multiple threads or tasks may be operating concurrently.
Each context is isolated; when you switch contexts, `tdbg` updates to show the
state of the newly selected context. This allows you to focus on and step
through the execution of discrete sections of your code, providing a powerful
tool to pinpoint and resolve issues effectively.

## Commands

### `connect [url] [registration_code]`
Connect to the debugger. Defaults are used if not provided.

### `reconnect`
Reconnect to the last debugger.

### `disconnect`
Disconnect from the current debugger.

### `watch expression`
Display the value of an expression in the watch area.

### `unwatch expression`
Remove an expression from the watch area.

### `pause`
Pause the current or next running context if none is active.

### `resume`
Resume execution in the current context.

### `view expression`
Output the value of an expression in the console. Shortcut: `v`

### `explore expression`
Open an expression in the data explorer. Shortcut: `x`

### `select`
Select the context to debug from available options.

### `next`
Move one step forward in execution. Shortcut: `n`

### `help`
Show help information.

### `quit`
Quit the debugger.

## Getting Help

To display a list of available commands and their descriptions, type `help` in the command input area.


## A sample Debugging Session

A typical debugging session with `tdbg` allows you to interactively step
through the execution of your application, inspect variables, and control the
execution flow. Here's how a basic session unfolds:

### Starting a Debug Session

1. Launch `tdbg` it will connect to the server to debug.
2. Issue the `pause` command, which tells the server to stop at the next
   possible breakpoint for any new job that is started. This may create a new
   execution context.

### Handling Contexts

- When a new context is generated, `tdbg` will log a message indicating that new contexts are available.
- Use the `select` command to view and choose from the available contexts. Choosing a context will load its current state into the debugger.

### Stepping Through Execution

- Once you've selected a context, you can step through the code using the
  `next` command. This steps through the execution flow one step at a time.
- After each step, the context's data will be updated, allowing you to monitor
  changes to variables and application state in real-time.

### Inspecting Variables

- To inspect a variable, use the `view` or `explore` command with the
  variable's name, prefixed by a dollar sign (e.g., `$person` for a variable
  named `person`).
- DTL's expression syntax is used here, enabling not just variable inspection
  but also the evaluation of expressions. For example, `strftime("%F", $date)`
  could be used to format a date.
- You can set up a watch for any variable or expression using the `watch`
  command, which will display its value in the watch area.

### Resuming Execution

- Issue the `resume` command to allow the remote system to continue execution.
  The system may either run to completion or stop at the next developer-defined
  pause point.

### Completing a Context

- When the current context completes, the screen will clear, signaling that you
  can select a new context if available.
- Debug log messages from the server will be displayed in the log area,
  providing insights into the execution process and issues.

### Ending the Debug Session

- To end your debugging session, simply issue the `quit` command.

Using the `pause`, `next`, `explore`, and `watch` commands in tandem allows you
to conduct a thorough inspection of how a process operates, offering you the
granularity needed to diagnose and fix issues effectively.
