// DebugLogger.js
const fs = require('fs');
const path = require('path');
const util = require('util');

// Singleton instance
let logStream;
const LOGFILE = process.env.TDBG_DEBUG_FILE

function getLogStream() {
    if (!logStream && typeof LOGFILE == 'string') {
        logStream = fs.createWriteStream(LOGFILE, { flags: 'a' });
    }
    return logStream;
}

function debug() {
    let args = Array.from(arguments);
    let message = "";
    let sep = "";
    args.forEach( arg => {
        let to_log = arg;
        if (typeof arg == 'object') {
            to_log = util.inspect(arg);
        }
        message += sep + to_log;
        sep = " ";
    })
    const timestamp = new Date().toISOString();
    let debug_file = getLogStream();
    if (debug_file) {
        debug_file.write(`[${timestamp}] ${message}\n`);
    }
}

module.exports = {
    debug,
};

