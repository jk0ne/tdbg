// Display_Area.js

const blessed = require('blessed');
const util = require('util');
const { EventEmitter } = require('events');
const { debug } = require('./DebugLogger.js');

class DisplayArea extends EventEmitter {
    constructor(screen, config) {
        super(); // Call the EventEmitter constructor

        // Default config
        const defaultConfig = {
            activeStyle: { bg: "#111111", track: { bg: "yellow", bold: true }}, //, scrollbar: { track: { bg: "blue" } } },
            inactiveStyle: { fg: 'white', bg: 'black', track: { bg: "#222222", bold: false} },
            string_decorations: {
                default: {
                    begin_string: '',
                    begin_origin: '{bold}', 
                    end_origin: '{/bold}',
                    end_string: '',
                },

                system: { 
                    begin_string: '',
                    begin_origin: '{white-fg}{bold}', 
                    end_origin: '{/bold}{/white-fg}',
                    end_string: '',
                },
                error: { 
                    begin_string: '{red-fg}',
                    begin_origin: '{bold}', 
                    end_origin: '{/bold}',
                    end_string: '{/red-fg}',
                },
                user: { 
                    begin_string: '{yellow-fg}',
                    begin_origin: '{bold}', 
                    end_origin: '{/bold}',
                    end_string: '{/yellow-fg}'
                },
                debug: { 
                    begin_string: '{#999999-fg}',
                    begin_origin: '{bold}', 
                    end_origin: '{/bold}',
                    end_string: '{/#999999-fg}'
                },
            },
            geometry: {
                bottom: 4,
                left: 0,
                width: '100%',
                height: '20%',
                top: 0,
            }
        };

        // Merge configs
        this.config = { ...defaultConfig, ...config };

        // Create the area
        this.area = blessed.text({
            parent: screen,
            bottom: this.config.geometry.bottom, 
            left: this.config.geometry.left,
            width: this.config.geometry.width,
            height: this.config.geometry.height,
            tags: true,
            style: Object.assign({}, this.config.inactiveStyle),
            scrollable: true,
            alwaysScroll: true,
            scrollbar: {
                ch: '#',
                track: {
                    bg: '#333333',
                },
            },
            keys: true,
            mouse: true,
        });
        this.autoscroll = true;
        this.area.on('scroll', () => {
            const scrollPosition = this.area.getScroll();
            const maxScrollPosition = this.area.getScrollHeight() - this.area.height;

            if (scrollPosition < maxScrollPosition) {
                this.autoscroll = false;    // Disable auto-scroll if the user scrolls up
            } else {
                this.autoscroll = true;    // Enable auto-scroll if the user scrolls to the bottom
            }
        });
        this.on('debug', this.debug_msg.bind(this));
        this.on('add_to_log', (msg) => { 
            //debug('received add_to_log:' + msg);
            this.update( msg.origin, msg.message ); 
        });
        this.area.on('focus', () => {
            this.focus();
        });
        this.area.on('blur', () => {
            this.blur();
        });
    }


    get_decorated(origin, data) {
        let decor = this.config.string_decorations[origin] || this.config.string_decorations.default;
        let message = data;
        let string;
        let begin_string = decor.begin_string;
        let end_string = decor.end_string;
        //debug('DATA IS11111'+ util.inspect(data));
        if (typeof data == 'object') {
            if (typeof data.pre == 'string') {
                begin_string = data.pre;
                end_string = data.post;
            }
            message = data.message
        }

        if (typeof origin != 'undefined') {
            string = begin_string + decor.begin_origin + origin + ":" + decor.end_origin + 
                     ' ' + message + end_string;
        } else {
            string = begin_string + message + end_string;
        }
        return string;
    }


    focus() {
        this.area.style.track = this.config.activeStyle.track;
        this.area.style.bg = this.config.activeStyle.bg;
        debug('style', util.inspect(this.area.style, {depth: null}));
        this.area.screen.render();
    }

    blur() {
        this.area.style.track = this.config.inactiveStyle.track;
        this.area.style.bg = this.config.inactiveStyle.bg;
        this.area.screen.render();
    }

    update(origin, newData) {
        //debug("stuff" + JSON.stringify(newData));
        this.emit('update', newData); // Emit an update event with the new data as an argument
        let decorated_message = this.get_decorated(origin, newData);
        this.area.pushLine(decorated_message);
        if (this.autoscroll) {
            this.area.setScrollPerc(100);
        }
        this.area.screen.render();
    }

    scroll(where) {
        this.area.scroll(where)
        this.area.screen.render();
    }

    scroll_page(where) {
        this.area.scroll((where * this.area.height) - 1);
        this.area.screen.render();
    }

    scroll_end(where) {
        this.area.setScrollPerc(100);
        this.autoscroll = true;
        this.area.screen.render();
    }

    debug_msg(args) {
        if(!Array.isArray(args)) {
            args = [ args ];
        }
        let message = "";
        let sep = "debug: ";
        args.forEach( arg => {
            message += sep + arg;
            sep = " ";
        })
        debug(message);
        this.update('debug', message)
    }

}

module.exports = DisplayArea;

