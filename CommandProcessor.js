const EventEmitter = require('events');
const { debug } = require('./DebugLogger.js');

class CommandProcessor extends EventEmitter {
    constructor(screen, config) {
        super();
        this.initCommands();
        this.on('command', (message) => {
            this.processCommand(message.command, message.args, message.origin);
        });
    }

    initCommands() {
        this.commands = {
            'connect': { 
                fn: (vars) => {
                    let message = {};
                    if (typeof vars[0] == 'string') {
                        message.url = vars[0];
                    }
                    if (typeof vars[1] == 'string') {
                        message.registration_code = vars[1];
                    }
                    this.send_event('web_socket:connect', message);
                },
                help: {
                    usage: 'connect [url] [registration_code]',
                    description: [
                        'Connect to debugger. If url and registration code are not provided,',
                        'previous url and registration code are used.'
                    ]
                }
            },
            'reconnect': { 
                fn: (vars) => {
                    this.send_event('web_socket:reconnect', {});
                },
                help: {
                    usage: 'reconnect',
                    description: [
                        'Reconnect to last debugger we were conected to. If debugger is ',
                        'connected, will disconnect first.'
                    ]
                }
            },
            'disconnect': { 
                fn: () => {
                    this.send_event('web_socket:disconnect', {});
                },
                help: {
                    usage: 'disconnect',
                    description: [
                        'Disconnects from currently connected debugger.'
                    ]
                }
            },
            'w': 'watch',
            'watch': {
                fn: (vars) => {
                    vars.forEach((varname) => {
                        this.send_event('watch_area:add_watch_item', varname);
                    });
                },
                help: {
                    usage: 'watch expression',
                    description: [
                        'Display the value of `expression` in the watch area.'
                    ]
                }
            },
            'unwatch': {
                fn: (varname) => {
                    this.send_event('watch_area:remove_watch_item', varname);
                },
                help: {
                    usage: 'unwatch expression',
                    description: [
                        'Removes expression from the watch area.'
                    ]
                }
            },
            'pause': {
                fn: () => {
                    this.send_event('debugger_command', { command: 'Debugger.pause', payload: {} });
                },
                help: {
                    usage: 'pause',
                    description: [
                        'Pauses current context or next running context if no context is active.'
                    ]
                }
            },
            'release': {
                fn: () => {
                    this.send_event('debugger_command', { command: 'Debugger.release_context', payload: {} });
                },
                help: {
                    usage: 'release',
                    description: [
                        'Release claim on current context.'
                    ]
                }
            },
            'resume': {
                fn: () => {
                    this.send_event('debugger_command', { command: 'Debugger.resume', payload: {} });
                },
                help: {
                    usage: 'resume',
                    description: [
                        'Resume execution in the current context.'
                    ]
                }
            },
            'v': 'view',
            'view': {
                fn: (args) => {
                    this.send_event('debugger_command', { command: 'view', payload: { varname: args[0] }});
                },
                help: {
                    usage: 'view expression',
                    description: [
                        'Output the value of expression (usually a $variable_name) in the console',
                        'shortcut: v'
                    ]
                }
            },
            'x': 'explore',
            'explore': {
                fn: (args) => {
                    this.send_event('debugger_command', { command: 'explore', payload: { varname: args[0] }});
                },
                help: {
                    usage: 'explore expression',
                    description: [
                        'Open the value of expression (usually a $variable_name) in the data explorer',
                        'shortcut: x'
                    ]
                }
            },
            'select': {
                fn: (args) => {
		    let listener = (type, contexts) => {
			//debug('select listener called');
			this.send_event('select_context', { contexts: contexts});
		    }
                    this.send_event('listen_once', { type: 'debugger_contexts_updated', listener: listener });
                    this.send_event('debugger_command', { command: 'Debugger.get_available_contexts', payload: {} });
                },
                help: {
                    usage: 'select',
                    description: [
                        'Select the context you want to debug from those available.'
                    ]
                }
            },
            'n': "next",
            'next': {
                fn: () => {
                    this.send_event('debugger_command', { command: 'Debugger.step_next', payload: {} });
                },
                help: {
                    usage: 'next',
                    description: [
                        'Move one step forward in execution.',
                        'shortcut: n' 
                    ]
                }
            },
            'help': {
                fn: () => {
                    this.show_help();
                },
                help: {
                    usage: 'help',
                    description: [
                        'Show this help.'
                    ]
                }
            },
            'quit': {
                fn: () => {
                    // TODO: Add a confirmation dialog here
                    this.send_event('system:shutdown_requested');
                },
                help: {
                    usage: 'quit',
                    description: [
                        'Quit the program.'
                    ]
                }
            }
        }
    }


    show_help() {
        let content = [
            '{cyan-fg}{bold}HotKeys{/bold}{/cyan-fg}:\n\n',
            '{bold}F1{/bold}:          This help\n',
            '{bold}tab{/bold}:         Switch between panes.\n',
            '{bold}shift-PgUp{/bold}:  Scroll console area one page up.\n',
            '{bold}shift-PgDn{/bold}:  Scroll console area one page down.\n',
            '{bold}shift-Up{/bold}:    Scroll console area up one line.\n',
            '{bold}shift-Dn{/bold}:    Scroll console area down one line.\n',
            '{bold}shift-End{/bold}:   Scroll console area to the bottom.\n',
            '{bold}ctrl-c{/bold}:      Quit tdbg\n',
            '\n\n',
            '{cyan-fg}{bold}Explore box{/bold}{/cyan-fg}:\n\n',
            '{bold}/{/bold}:        Search within content.\n',
            '{bold}f{/bold}:        Toggle flattenned data view.\n',
            '{bold}Shift-s{/bold}:  Save content to file.\n',
            '{bold}Escape{/bold}:   close explore box.\n', 
            '\n\n',
            '{cyan-fg}{bold}Watch box{/bold}{/cyan-fg}:\n\n',
            '{bold}Up/Down{/bold}:  Change selection.\n',
            '{bold}Enter{/bold}:    Open data in Explore pane.\n',
            '\n\n',
            '{cyan-fg}{bold}Command area{/bold}{/cyan-fg}:\n\n',
            '{bold}Up/Down{/bold}:     View command history\n',
            '{bold}Home/Ctrl-a{/bold}: Move to beginning of command.\n',
            '{bold}End/Ctrl-e{/bold}:  Move to end of command.\n',
            '{bold}Ctrl-w{/bold}:      Delete word left of cursor.\n',
            '\n\n',
            '{cyan-fg}{bold}Available Commands{/bold}{/cyan-fg}: \n\n',
        ];
        Object.keys(this.commands).forEach( (name) => {
            if (typeof this.commands[name] == 'object') {
                let help = this.commands[name].help;
                let help_text = "  {bold}" + help.usage + "{/bold}\n";
                help.description.forEach( (line) => {
                    help_text += '      ' + line + "\n";
                })
                help_text += "\n";
                content.push(help_text);
            }
        });
        let message = {
            label: 'tdbg help',
            data: content.join(''),
            allow_flatten: false,
            dont_escape: true
        };
        this.send_event('explore_box:open', message);
    }

    send_event(type, payload) {
        this.emit('message', { type: type, data: payload });
    }

    processCommand(cmd, args) {
        let command = this.commands[cmd];
        let cmd_string = cmd;
        let display_args = "";
        if (Array.isArray(args) ) {
            display_args = args.join(' ');
        }
        if (typeof command == 'string' && typeof this.commands[command] == 'object') {
            cmd_string = command;
            command = this.commands[command];
        }
        this.log('> {green-fg}' + cmd_string + " " + display_args + '{/green-fg}')
        if (command) {
            command.fn(args);
        } else {
            // Handle unknown commands here, if you like
            this.log('{red-fg}Unknown Command: ' + cmd_string +'{/red-fg}')
            this.send_event('system:unknown_command', `Unknown command: ${cmd}`);
        }
    }

    log(message) {
        this.send_event('log', { message: message });
    }

}

module.exports = CommandProcessor;
