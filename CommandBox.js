const blessed = require('blessed');
const util = require('util');
const EventEmitter = require('events');
const { debug } = require('./DebugLogger.js');

class CommandBox extends EventEmitter {
    constructor(screen, config) {
        super();


        const defaultConfig = {
            activeStyle: { border: { fg: 'yellow', bold: true } },
            inactiveStyle: { border: { fg: 'grey', bold: false } },
            cursorStyle: {
                'on': {
                    start: '{#FFFF00-bg}{#000000-fg}',
                    end: '{/#000000-fg}{/#FFFF00-bg}',
                },
                'off': {
                    start: '',
                    end: ''
                }
            }
            // Add more default configs here
        };

        this.config = { ...defaultConfig, ...config };
        this.commandHistory = [];
        this.historyIndex = -1;
        this.interval;

        this.data = '';
        this.cursorPos = 0;
        this.screen = screen;
        this.cursor_blink= 'on';

        // Set up the blessed box
        this.area = blessed.box({
            parent: screen,
            bottom: 1,
            left: 0,
            height: 3,
            width: '100%',
            tags: true,
            //      mouse: true,
            //      clickable: true,
            border: { type: 'line' },
            //keys: true,
        });

        // this.screen.append(this.area);
        this.area.setFront();
        const hotkeys = [
            'left',
            'right',
            'backspace',
            'delete',
            'home',
            'C-a',
            'end',
            'C-e',
            'C-w'
        ];

        // Input handling
        this.area.key(hotkeys, (ch, key) => {
            switch (key.full) {
                case 'left':
                    this.cursorLeft();
                    break;
                case 'right':
                    this.cursorRight();
                    break;
                case 'backspace':
                    this.backspace();
                    break;
                case 'delete':
                    this.delete();
                    break;
                case 'home':
                case 'C-a':
                    this.cursorStart();
                    break;
                case 'end':
                case 'C-e':
                    this.cursorEnd();
                    break;
                case 'C-w':
                    this.removeWordBeforeCursor();
                    break;
            }
            this.render();
        });


        const keypress_handler = function(ch, key) {
            if (key.name == 'enter') {
                this.handleSubmit()
            } else {
                this.handle_keypress(ch, key);
            }
        }.bind(this)

        this.area.key(['up', 'down'], (ch, key) => this.handleKeyHistory(ch, key, screen));
        this.area.on('mouse', (event) => {
            debug('MOUSE: ' + util.inspect(event));
        });

        this.area.on('focus', () => {
            //debug('INPUT FOCUS');
            this.area.on('keypress', keypress_handler);
            clearInterval(this.interval);
            this.area.style = this.config.activeStyle;
            this.interval = setInterval(() => { this.blink_cursor(); }, 600);
            this.render();
        });

        this.area.on('blur', () => {
            //debug('INPUT BLUR');
            this.area.removeListener('keypress', keypress_handler);
            this.area.style = this.config.inactiveStyle;
            clearInterval(this.interval);
            this.cursor_blink = 'on';
            this.render();

        })

//    this.area.key('enter', () => this.handleSubmit());

        this.area.focus();
        this.render();
    }

    handle_keypress(ch, key) {
        if (this.isPrintableKey(key)) {
            this.addData(ch);
            this.render();
        }
    }

    isPrintableKey(key) {
        // List of non-printable key names
        const nonPrintableKeys = new Set([
            'backspace', 'escape', 'tab', 'return', 'enter', 'shift', 'control',
            'alt', 'pause', 'capslock', 'pageup', 'pagedown', 'end', 'home',
            'left', 'up', 'right', 'down', 'insert', 'delete', 'f1', 'f2', 'f3',
            'f4', 'f5', 'f6', 'f7', 'f8', 'f9', 'f10', 'f11', 'f12'
        ]);

        // Check for control characters or meta keys
        if (key.ctrl || key.meta) {
            return false;
        }

        // Check for non-printable keys
        if (nonPrintableKeys.has(key.name)) {
            return false;
        }

        // If all checks passed, the key is printable
        return true;
    }


    blink_cursor() {
        if (this.cursor_blink == 'on') {
            this.cursor_blink = 'off';
            //        debug('blink: ' + this.cursorPos + ' ' + JSON.stringify(this.data));
        } else {
            this.cursor_blink = 'on';
        }
        this.render();
    }

    addData(ch) {
        this.data = [this.data.slice(0, this.cursorPos), ch, this.data.slice(this.cursorPos)].join('');
        this.cursorPos++;
    }

    cursorStart() {
        this.cursorPos = 0;
    }

    cursorEnd() {
        this.cursorPos = this.data.length;
    }
    cursorLeft() {
        if (this.cursorPos > 0) this.cursorPos--;
    }

    cursorRight() {
        if (this.cursorPos < this.data.length) this.cursorPos++;
    }

    backspace() {
        if (this.cursorPos > 0) {
            this.data = this.data.substring(0, this.cursorPos - 1) + this.data.substring(this.cursorPos);
            this.cursorPos--;
        }
    }

    delete() {
        if (this.cursorPos < this.data.length) {
            this.data = this.data.substring(0, this.cursorPos) + this.data.substring(this.cursorPos + 1);
        }
    }

    removeWordBeforeCursor() {
        let text = this.data;
        let cursorPosition = this.cursorPos;

        // Check if there's text before the cursor position
        if (cursorPosition === 0) {
            return;  // Nothing to remove
        }

        // Find the index of the start of the word to be removed
        let start = cursorPosition - 1;

        // If the cursor is on a space, skip the spaces to find the start of the word
        while (start > 0 && this.data[start] === ' ') {
            start--;
        }

        // Now find the beginning of the word
        while (start > 0 && this.data[start - 1] !== ' ') {
            start--;
        }

        // Construct the new text and calculate new cursor position
        this.data = this.data.substring(0, start) + this.data.substring(cursorPosition);
        this.cursorPos = start;
    }


    handleSubmit() {
        this.data = this.data.replace('\r', '');
        let text = this.data;
        // debug('FOOOOOOOOOOOOOOOOOOOOO', JSON.stringify(text));
        this.commandHistory.push(text);
        this.historyIndex = -1;
        this.data = '';
        this.cursorPos = 0;
        this.area.setContent('> ');
        if (text.length > 0) {
            this.send_event('local_command', this.parseCommand(text));
        }
        this.render();
    }

    render() {
        let beforeCursor = this.data.substring(0, this.cursorPos);
        let atCursor = this.data.substring(this.cursorPos, this.cursorPos + 1) || ' '; // Default to space if at end
        let afterCursor = this.data.substring(this.cursorPos + 1) + ' ';
        let cursor = this.config.cursorStyle[this.cursor_blink];

        //let displayData = beforeCursor + '{reverse}' + atCursor + '{/reverse}' + afterCursor;
        let displayData = beforeCursor + cursor.start + atCursor + cursor.end + afterCursor;
        this.area.setContent('> ' +displayData);
        this.screen.render();
    }


    send_event(type, payload) {
        setImmediate( () => {
            this.emit('message', { type: type, data: payload });
        });
    }

    parseCommand(input) {
        // This regex should match anything enclosed in quotes or any word not containing spaces.
        const regex = /"([^"]+)"|'([^']+)'|(\S+)/g;
        const args = [];
        let match;

        while ((match = regex.exec(input)) !== null) {
            args.push(match[1] || match[2] || match[3]);
        }

        if (args.length === 0) {
            return { command: null, arguments: [] };
        }

        const command = args.shift(); // The first argument is the command
        return { command, args };
    }



    handleKeyHistory(ch, key, screen) {
	if (key.name === 'up' && this.historyIndex < this.commandHistory.length - 1) {
	    this.historyIndex++;
	} else if (key.name === 'down' && this.historyIndex > -1) {
	    this.historyIndex--;
	}

	const historyCmd = this.commandHistory[this.commandHistory.length - 1 - this.historyIndex];
	if (historyCmd !== undefined) {
	    this.data = historyCmd;
	    this.cursorPos = historyCmd.length;
	} else {
	    this.data = '';
	}
	this.render();
    }
    getCommandHistory() {
        return this.commandHistory;
    }

    setCommandHistory(history) {
        this.commandHistory = history;
    }
}

// Usage:
// const screen = blessed.screen();
// const inputLine = new SingleLineInput(screen, { top: '50%', left: 'center', width: '50%' });
// screen.render();


module.exports = CommandBox;
