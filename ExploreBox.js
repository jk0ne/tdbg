const blessed = require('blessed');
const DTL = require('dtl-js');
const fs = require('fs');
const { EventEmitter } = require('events');
const { debug } = require('./DebugLogger.js');

function showMessage(screen, text) {
    const messageBox = blessed.message({
        parent: screen,
        top: 'center',
        left: 'center',
        width: '50%',
        height: 'shrink',
        style: {
            fg: 'white',
            bold: true,
            border: { fg: 'red', bold: true }
        },
        border: { type: "line" },
        label: ' Message ',
        tags: true,
        keys: true,
        hidden: true,
    });

    messageBox.display(text, 0, () => {
        messageBox.destroy(); // Destroy the screen after the user dismisses the message
    });
}


class ExploreBox extends EventEmitter {
    constructor(screen, config) {
        super();

        const defaultConfig = {
            activeStyle: { border: { fg: 'green' } },
            inactiveStyle: { border: { fg: 'white' } },
            // ... more default configurations
        };

        // Merge default and user-provided config
        this.config = { ...defaultConfig, ...config };

        this.screen = screen;
        this.data = "";
        this.flatten = false;
        this.escape_content = true;
        this.allow_flatten = true;

        // Create the box with initial settings
        this.area = blessed.box({
            parent: this.screen,
            top: 'center',
            left: 'center',
            width: '80%',
            height: '80%',
            keys: true,
            mouse: true,
            scrollable: true,
            alwaysScroll: true,
            hidden: true,
            tags: true,
            padding: {
                left: 2,
                right: 2,
                top: 1,
                bottom: 1,
            },
            scrollbar: {
                ch: '#',
                track: {
                    bg: 'yellow',
                },
            },
            style: {
                focus: {
                    border: {
                        fg: 'yellow'
                    }
                }
            },
            border: 'line',
            label: ' Explore '
        });

        // Attach escape key listener
        this.area.key(['escape'], () => this.hide());
        this.area.key(['f'], () => this.toggle_flatten());
        this.area.key(['S-s'], () => this.save_data());
        this.area.key(['pageup', 'pagedown'], (ch, key) => {
            if (key.full == 'pageup') {
                this.scroll_page(-1);
            } else if (key.full == 'pagedown') {
                this.scroll_page(1);
            }
        });
        this.area.key(['/'], (ch, key) => {
            const searchPrompt = blessed.prompt({
                parent: screen,
                top: 'center',
                left: 'center',
                height: 'shrink',
                width: '50%',
                keys: true,
                style: {
                    fg: 'white',
                    bold: true,
                    border: { fg: 'red', bold: true }
                },
                border: { type: "line" },
            });

            searchPrompt.input('Search for:', '', (err, value) => {
                if (value) {
                    this.search = value;
                } else {
                    this.search = undefined;
                }
                searchPrompt.destroy(); // Make sure to clean up the prompt
                this.update_content()
                //screen.render();
            });

            screen.render();
        });

        this.area.on('focus', () => { 
            //debug('EXPLORE focus');
        })
        this.area.on('blur', () => { 
            //debug('EXPLORE BLUR');
        })
        this.on('open', (message) => {
            //debug('ExploreBox open', message);
            if (message.dont_escape) {
                this.escape_content = false;
            } else {
                this.escape_content = true;
            }
            if (message.allow_flatten == false) {
                this.allow_flatten = false
            } else {
                this.allow_flatten = true;
            }
            this.set_label(message.label);
            this.set_data(message.data);

            this.label = message.label || ' Explore ';
            this.show();
        });
        this.on('close', () => {
            this.search = undefined;
            this.hide();
        });
    }

    scroll_page(where) {
        this.area.scroll((where * this.area.height) - 1);
        this.area.screen.render();
    }

    set_label(label) {
        if (typeof label == 'string') {
            this.label = label
        } else {
            this.label = ' Explore ';
        }
        this.area.setLabel({ text: ' {bold}' + label + '{/bold} ', side: "left" });
    }

    // Method to populate and show the box
    show(label) {
        if (typeof label == 'string') {
            this.set_label(label);
        }
        //debug('ExploreBox show');
        this.screen.saveFocus();
        this.area.setFront();
        this.area.show();
        this.area.focus();
        this.screen.render();
    }

    set_data(data) {
        //debug('ExploreBox set_data', data);
        this.data = data;
        this.update_content();
    }

    update_content() {
        let newContent = '';
        // Split the current data by new line to process it line by line.
        const lines = this.get_content().split('\n');

        // Create a regex from the search string (if it exists)
        const searchRegex = this.search ? new RegExp(this.search, 'g') : null;
        let firstMatchPosition = -1;

        // Process each line
        lines.forEach((line, index) => {
            if (searchRegex) {
                // Replace the matching search term with highlighted text
                line = line.replace(searchRegex, (match, offset) => {
                    // Store the position of the first match to scroll to it later
                    if (firstMatchPosition === -1) {
                        firstMatchPosition = index - 3;
                        if (firstMatchPosition < 0) {
                            firstMatchPosition = 0;
                        }
                    }
                    return `{cyan-bg}{black-fg}${match}{/black-fg}{/cyan-bg}`;
                });
            }
            // Add the processed line to the new content
            newContent += line + '\n';
        });

        // Set the content of the box with the highlighted text
        this.area.setContent(newContent);

        // If there was a match, scroll to the first occurrence
        if (firstMatchPosition !== -1) {
            this.area.scrollTo(firstMatchPosition);
        }

        // Render the screen to reflect the changes
        this.area.screen.render();
    }

    get_content() {
        let data;
        if (typeof this.data == 'undefined' || this.data === null) {
            this.content = "";
        } else if (typeof this.data != 'string') {
            if (this.allow_flatten && this.flatten) {
                data = this.get_flattened();
            } else {
                data = this.data
            }
            if (this.escape_content) {
                this.content = blessed.escape(JSON.stringify(data, null, 2));
            } else {
                this.content = JSON.stringify(data, null, 2);
            }
        } else {
            if (this.escape_content) {
                this.content = blessed.escape(this.data);
            } else {
                this.content = this.data;
            }
        }
        return this.content;
    }


    get_flattened() {
        let data = this.data;
        let label = this.label || ' ';
        if (this.label.charAt(0) == '$' && this.label != '$.') {
            const keys = this.label.slice(1).split('.');
            // Start with the innermost value
            let nestedObject = this.data;
            // Iterate over the keys array in reverse order and build the object
            for (let i = keys.length - 1; i >= 0; i--) {
                nestedObject = { [keys[i]]: nestedObject };
            }
            data = nestedObject;
        }
            // Return the fully constructed object
        return DTL.apply(data, "(: flatten($.) :)");
    }

    toggle_flatten() {
        if (this.flatten) {
            this.flatten = false;
        } else {
            this.flatten = true;
        }
        this.update_content();
        this.screen.render();
    }

    save_data() {
        const prompt = blessed.prompt({
            parent: this.screen,
            left: 'center',
            top: 'center',
            width: '50%',
            height: 'shrink',
            style: {
                fg: 'white',
                bold: true,
                border: { fg: 'red', bold: true }
            },
            border: { type: "line" },
            label: ' Save As ',
            tags: true,
            keys: true,
            vi: true
        });

        prompt.input('Enter a filename to save:', '', (err, value) => {
            if (err || !value) {
                showMessage(this.screen, 'No filename entered.');
                return;
            }

            try {
                // Attempt to write to the file synchronously
                // Replace 'Your data here' with the actual data you want to write
                fs.writeFileSync(value, JSON.stringify(this.data, null, 4), 'utf8');
                showMessage(this.screen, `File: ${value} saved successfully.`);
            } catch (error) {
                showMessage(this.screen, `Unable to save ${value}: ${error.message}`);
            }
            process.nextTick(() => {prompt.destroy()} );
        });
        
    }


    // Method to hide the box
    hide() {
        //debug('ExploreBox hide');
        this.screen.restoreFocus();
        this.area.setContent('');
        this.area.hide();
        this.screen.render();
        this.emit('hide');
    }
}

module.exports = ExploreBox;
