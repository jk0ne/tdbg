const EventEmitter = require('events');
const util = require('util');
const DTL = require('dtl-js');
const { debug } = require('./DebugLogger.js');
const BAD_INPUT = Symbol.for('bad_input');

function adjustIndentation(codeStr, column, indentation) {
    let indent = ''
    if (typeof indentation == 'number') {
        indent = ' '.repeat(indentation);
    }
    // Split the string into an array of lines
    let lines = codeStr.split('\n');

    // Create a regular expression to match the leading spaces
    let regex = new RegExp(`^ {${column}}`);

    // Replace the leading spaces on each line
    lines = lines.map(line => line.replace(regex, indent));

    // Join the lines back into a single string
    return indent + lines.join('\n');
}

function get_source_log_message(source_info) {
    let name = source_info.name || '';
    let location = source_info.location;
    let text = source_info.text;
    let log_message = "";
    if (typeof location == 'object') {
        log_message = `${location.source}:${location.start.line}\n`;
    }
    if (typeof text == 'string') {
        log_message += "{bold}" + adjustIndentation(text, location.start.column-1, 2) + "{/bold}";
    }
    return log_message;
}

class RemoteDebuggerHandler extends EventEmitter {
    constructor(config) {
        super();
        this.watched_contexts = {};
        this.available_contexts = {};
        this.client_state = {}; 
        this.active_context;
        this.log_activity = config.log_activity || false;

        this.init_debugger_actions();
        this.init_debugger_commands();

        this.on('debugger_message_received', (message) => {
            //debug('message received by DebuggerHandler', message);
            this.handleDebuggerMessageReceived(message);
        });
        this.on('command', (message) => {
            //debug('command received by DebuggerHandler', message);
            this.handleDebuggerCommandReceived(message);
        });
        this.on('debugger_connected', (message) => {
            //debug('debugger_connected received by DebuggerHandler', message);
            this.client_state.client_id = message.client_id;
            this.client_state.access_key = message.access_key;
            this.send_to_debugger('Debugger.enable', {}) 
        });

        this.on('debugger_disconnected', (message) => {
            //debug('debugger_connected received by DebuggerHandler', message);
            this.active_context = undefined;
            this.watched_contexts = {};
            this.available_contexts = {};
        });
    }

    init_debugger_actions() {
        this.debugger_actions = {
            'Debugger.enabled': (payload) => { 
                this.handleDebuggerEnabled(payload);
            },
            'Debugger.paused': (payload) => { 
                this.handleDebuggerPaused(payload);
            },
            'Debugger.resumed': (payload) => { 
                this.handleDebuggerResumed(payload);
            },
            'Debugger.got_source': (payload) => { 
                //debug('GOT_SOURCE?' + util.inspect(payload));
                this.handleDebuggerGotSource(payload);
            },
            'Debugger.context_claimed': (payload) => { 
                debug('Context Claimed' + util.inspect(payload));
                this.handleDebuggerContextClaimed(payload);
            },
            'Debugger.log': (payload) => { 
                //debug('Debugger.log:' + util.inspect(payload));
                this.handleDebuggerLog(payload);
            },
            'Debugger.available_contexts': (payload) => { 
                debug('AVAILABLE_CONTEXTS' + util.inspect(payload));
                this.handleDebuggerAvailableContexts(payload);
            },
            'Debugger.script_parsed': (payload) => { 
                this.handleDebuggerScriptParsed(payload);
            },
            'Debugger.state_updated': (payload) => { 
                this.handleDebuggerStateUpdated(payload);
            },
            'Debugger.execution_started': (payload) => { 
                debug('Execution started with context', payload.context_id);
                // send execution started to debugger
            },
            'Debugger.execution_finished': (payload) => { 
                debug('Execution finished with context', payload.context_id);
                this.handleExecutionFinished(payload);
            },
        };
    }

    init_debugger_commands() {
        this.debugger_commands = {
            'Debugger.enable': (payload) => { 
                this.send_to_debugger('Debugger.enable', payload) 
            },
            'Debugger.pause': (payload) => { 
                if (!payload.context_id) {
                    payload.context_id = this.active_context;
                }
                this.send_to_debugger('Debugger.pause', payload) 
            },
            'Debugger.resume': (payload) => { 
                if (!payload.context_id) {
                    payload.context_id = this.active_context;
                }
                if (payload.context_id) {
                    this.send_to_debugger('Debugger.resume', payload) 
                } else {
                    this.log("{red-fg}Can't resume: no context selected{/red-fg}")
                }
            },
            'Debugger.step_next': (payload) => { 
                if (!payload.context_id) {
                    payload.context_id = this.active_context;
                }
                if (payload.context_id) {
                    this.send_to_debugger('Debugger.step_next', payload) 
                } else {
                    this.log("{red-fg}Can't step: no context selected{/red-fg}")
                }
            },
            'Debugger.get_available_contexts': (payload) => { 
                //debug('GET available_contexts!1111' + util.inspect(payload));
                this.send_to_debugger('Debugger.get_available_contexts', payload);
            },
            'Debugger.claim_context': (payload) => { 
                //debug('claim context' + util.inspect(payload));
                this.send_to_debugger('Debugger.claim_context', payload);
            },
            'Debugger.release_context': (payload) => { 
                if (!payload.context_id) {
                    payload.context_id = this.active_context;
                }
                if (payload.context_id) {
                    this.send_to_debugger('Debugger.release_context', payload) 
                } else {
                    this.log("{red-fg}Can't release context: no context selected{/red-fg}")
                }
            },
            'Debugger.get_source': (payload) => { 
                //debug('GET SOURCE!1111' + util.inspect(payload));
                this.send_to_debugger('Debugger.get_source', payload);
            },
            'view': (payload) => {
                let value = this.parseVariable(payload.varname);
                if (value == BAD_INPUT) {
                    this.send_event('log', { message: "view: Unable to parse " + payload.varname });
                } else {
                    this.send_event('log', { message: payload.varname + ": " + JSON.stringify(value, null, 2)});
                }
            },
            'explore': (payload) => {
                let value = this.parseVariable(payload.varname);
                if (value == BAD_INPUT) {
                    this.send_event('log', { message: "explore: Unable to parse " + payload.varname });
                } else {
                    this.send_event('explore', { label: payload.varname, data: value });
                }
            },
            'switch_context': (payload) => {
                let context_id = payload.context_id;
                this.active_context = context_id;
                let context_info = this.available_contexts[context_id];

                // either we already have this one, or we need to create it.
                debug('available_context:' + util.inspect(this.available_contexts[context_id], { depth: null}));
                this.send_to_debugger('Debugger.claim_context', { context_id: context_id});
                
                if (this.watched_contexts[this.active_context] && this.watched_contexts[this.active_context].log_lines > 0) {
                    let message = this.watched_contexts[this.active_context].log_lines.join('\n')
                    this.log(message)
                }
            }
        };
    }

    create_watch_context(context_id) {
        this.watched_contexts[context_id] = {
            paused: false,
            remote_state: {},
            log_lines: [],
        };
        return this.watched_contexts[context_id];
    }

    handleDebuggerMessageReceived(event) {
        const { type, payload } = event;
        const handler = this.debugger_actions[type];

        this.message_logger('from_server', type, payload);

        if (handler) {
            handler(payload);
        } else {
            this.send_event('log', `Unknown debugger message type: ${type}`);
        }
    }

    handleDebuggerCommandReceived(event) {
        const { command, payload } = event;
        const handler = this.debugger_commands[command];

        this.message_logger('from_client', command, payload);

        if (handler) {
            handler(payload);
        } else {
            this.send_event('log', `Unknown debugger message type: ${command}`);
        }
    }

    // Handlers for each event type
    handleDebuggerEnabled(payload) {
        //this.log(`Received Debugger.enabled with payload: ${JSON.stringify(payload)}`);
        this.log('Remote Debugger enabled');
    }

    handleDebuggerPaused(payload) {
        let context_id = payload.state.context_id;
        let was_paused = false;
        if (!this.active_context) {
            this.active_context = context_id;
        }
        if (this.watched_contexts[context_id]) {
            was_paused = this.watched_contexts[context_id].paused;
            this.watched_contexts[context_id].paused = true;
        } else if (Object.keys(this.watched_contexts).length == 0) {
            // if we aren't watching ANY contexts and we receive a paused
            // message, create a watch context and start watching it
            this.create_watch_context(context_id);
            this.watched_contexts[context_id].paused = true;
        }
        //this.log(`Received Debugger.paused with payload: ${JSON.stringify(payload)}`);
        if (!was_paused) {
            this.log('Debugger paused');
        }
    }

    handleDebuggerResumed(payload) {
        if (this.watched_contexts[payload.context_id]) {
            this.watched_contexts[payload.context_id].paused = true;
        }
        //this.log(`Received Debugger.resumed with payload: ${JSON.stringify(payload)}`);
        //this.log('Debugger resumed');
    }

    handleDebuggerAvailableContexts(contexts) {
        let old_available_contexts = this.available_contexts;
        let contexts_changed = false;
        
        if (Array.isArray(contexts)) {
            this.available_contexts = {};
            contexts.forEach( (item) => {
                this.available_contexts[item.context_id] = item;
                if (!this.watched_contexts[item.context_id]) {
                    // if we don't have a watched context for this
                    // context id, create one so we have a place
                    // to store anything that comes in. It will get
                    // cleaned out if an available context becomes 
                    // unavailable
                    this.create_watch_context(item.context_id);
                }
                if (!old_available_contexts[item.context_id]) {
                    contexts_changed = true;
                } else {
                    delete old_available_contexts[item.context_id];
                }
            });
            if (contexts_changed || Object.keys(old_available_contexts).length > 0) {
                this.log('New contexts available');
                if (!this.active_context) {
                    this.send_event('select_context', { contexts: this.available_contexts });
                }
            }
        }
	//debug('AVAILABLE: ' + util.inspect(this.available_contexts));
        // sync watched_contexts with available contexts:
        let watched_ids = Object.keys(this.watched_contexts);
        watched_ids.forEach( (id) => {
            if (typeof this.available_contexts[id] == 'undefined') {
                this.removeWatchedContext(id);
            }
        });
        this.send_event('debugger_contexts_updated', this.available_contexts);
    }

    handleDebuggerContextClaimed(payload) {
        debug('ContextClaimed' + payload); 
    }

    handleDebuggerGotSource(payload) {
        // We do nothing on script parsed. Maybe later we cache it so breakpoints can
        // be selected;
        this.send_event('debugger_got_source', payload);
        // this.log( `Received Debugger.script_parsed with payload: ${JSON.stringify(payload)}`);
    }
    
    handleDebuggerLog(payload) {
        // if our active context is this context, we emit the logs
        // if it's not, we store them 
        if (this.active_context == payload.context_id) {
            let log_data = {
                message: payload.log_lines.join('\n'),
                pre: ">> {bold}{white-fg}",
                post: "{/white-fg}{/bold}"
            }
            this.log(log_data);
        } else {
            if (this.watched_contexts[payload.context_id]) {
                payload.log_lines.forEach( (line) => {
                    this.watched_contexts[payload.context_id].log_lines.push(line);
                });
            }
        }

    }

    handleDebuggerScriptParsed(payload) {
        // We do nothing on script parsed. Maybe later we cache it so breakpoints can
        // be selected;
        this.send_event('debugger_script_parsed', payload);
        // this.log( `Received Debugger.script_parsed with payload: ${JSON.stringify(payload)}`);
    }

    handleDebuggerStateUpdated(payload) {
        debug("FOOBAR" + util.inspect(payload, { depth: null}));
        debug('BAZBAT' + util.inspect(this.watched_contexts, {depth:null}));
        let context = this.watched_contexts[payload.state.context_id];
        if (context) { 
            context.remote_state = payload.state;
            context.paused = payload.state.paused;
        }
//        if (typeof payload.state.source_info == 'object') {
            //this.log(get_source_log_message(payload.state.source_info));
//        }
        this.send_event('debugger_state_updated', payload.state);
        this.send_event('watch_area:update_watch_data', payload.state.scope);
    }

    handleExecutionFinished(payload) {
        if (this.active_context == payload.context_id) {
            this.active_context = undefined;
        }
        this.removeWatchedContext(payload.context_id);
        this.send_event('execution_finished', payload);
    }

    removeWatchedContext(context_id) {
        if (this.active_context == context_id) {
            this.active_context = undefined;
        }
        delete this.watched_contexts[context_id];
    }

    parseVariable(varname) {
        debug('Parse:' + util.inspect(this.watched_contexts) + this.active_context );
        let parsed = "N/A";
        let context = this.watched_contexts[this.active_context];
        if (context && typeof context.remote_state == 'object') {
            try {
                parsed = DTL.apply(context.remote_state.scope, { "out": "(: " + varname + " :)"});
            } catch(e) {
                parsed = BAD_INPUT;
            }
        }
        return parsed;
    }

    send_event(type, payload) {
        this.emit('message', { type: type, data: payload });
    }

    send_to_debugger(type, payload) {
        this.send_event('send_debugger_message', { type: type, payload: payload });
    }

    log(message) {
        this.send_event('log', { message: message });
    }

    message_logger(direction, type, payload) {
        if (this.log_activity) {
            let message = {
                direction: direction,
                when: Date.now(),
                type: type,
                payload: payload
            }
            debug('MSGLOG: '+ JSON.stringify(message));
        }
    }
}

module.exports = RemoteDebuggerHandler;
