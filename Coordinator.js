const EventEmitter = require('events');
const util = require('util');
const { debug } = require('./DebugLogger.js');

class Coordinator extends EventEmitter {
    constructor(screen) {
        super();
        this.modules = {}; // To hold references to all registered modules
        this.log_module = null;
        this.core_actions = {};
        this.screen = screen
        this.temp_listeners = {};

        this.init_core_actions();

        this.on('message', (payload) => {
            this.handle_module_message('core', payload);
        });
    }

    /**
     * Register a new module with the Coordinator
     * @param {string} moduleName - The name of the module to be registered
     * @param {Object} moduleInstance - The instance of the module
     */
    register_module(moduleName, moduleInstance) {
        // Save a reference to the module
        //debug('Module added'+ moduleName);
        this.modules[moduleName] = moduleInstance;

        // Listen for the 'message' event on the module
        moduleInstance.on('message', (payload) => {
            this.handle_module_message(moduleName, payload);
        });
    }

    send_event(type, payload) {
        this.emit('message', { type: type, data: payload });
    }

    /**
     * Handle messages coming from registered modules
     * @param {string} moduleName - The name of the module that sent the message
     * @param {Object} payload - The payload of the message
     */
    handle_module_message(moduleName, payload) {
        const { type, data } = payload;
        //debug(`Received message of type ${type} from ${moduleName}`);

        // Auto-route the message
        let targetModule = "";
        let action = type;
        // change how we route. We try to handle it in core first
        // if it returns false, it doesn't have a handler and should
        // fall through.
        if (typeof this.core_actions[type] != 'undefined') {
            this.handle_core_message(type, data);
        } else {
            if (/:/.test(type)) {
                [targetModule, action] = type.split(':');
            }
            // don't send messages from the module to itself, it already knows
            // how to do that, if it sends with it's own name, it is an outbound
            // message intended for others to hear.
            if (targetModule != moduleName && this.modules[targetModule]) {
                //debug(`Routing message to ${targetModule}`);
                this.modules[targetModule].emit(action, data);
            } else {
                // message was not targeted and we don't have a handler.
                // emit the event on ourself in case someone needs it.
                //debug(`emitting on coordinator ${type}`);
                this.emit(type, data);
            }
        }
    }

    handle_core_message(type, data) {
        let thing_to_do = this.core_actions[type];
        if (typeof thing_to_do == 'function') {
            thing_to_do(data)
        }
        if (Array.isArray(this.temp_listeners[type])) {
            // loop over this.temp_listeners[type] and for each object
            // call emit('type', data) on it.
            while (this.temp_listeners[type].length > 0) {
		const item = this.temp_listeners[type].pop(); // Remove the last item from the array

		if (typeof item === 'function') {
		    // If the item is a function, call it with (type, data)
		    item(type, data);
		} else if (item && typeof item.emit === 'function') {
		    // If the item is an object with an 'emit' method, call it with (type, data)
		    item.emit(type, data);
		}
	    }
	}
    }


    set_log_module(moduleName) {
        this.log_module = this.modules[moduleName];
    }

    init_core_actions() {
        this.core_actions = {
            'debug': (message) => {
                if(this.log_module) {
                    this.log_module.emit('debug', message);
                }
            },
            'log': (message) => {
                //debug('log received', message);
                if(this.log_module) {
                    this.log_module.emit('add_to_log', message);
                }
            },
            'listen_once': (message) => {
    		this.add_temporary_listener(message.type, message.listener);
            },
            'explore': (message) => {
                this.send_event('explore_box:open', message);
            },
            'select_context': (message) => {
                debug('select_context' + util.inspect(message));
                let handler = (item) => {
                    //debug('context selected' + util.inspect(item));
                    this.send_event('debugger_command', { command: 'switch_context', payload: { context_id: item.item.context_id } });
                };
                let list_entries = [];
                Object.keys(message.contexts).forEach( (key) => {
                    let item = message.contexts[key];
                    let list_entry = {
                        text: item.source_info.location.source + ":" + item.source_info.location.start.line + " (" + key +")",
                        item: item
                    }
                    list_entries.push(list_entry);
                });
                this.send_event('select_list:open', { label: ' Contexts Available ', items: list_entries, on_select: handler} );
            },
            'get_source': (message) => {
                this.send_debugger_command('Debugger.get_source', message);
            },
            'debugger_connected': (message) => {
                //debug('debugger_connected received', util.inspect(message));
                this.send_event('log', { origin: 'core', message: "connected with client_id " + message.client_id});
                this.send_event('remote_debugger:debugger_connected', message);
            },
            'local_command': (command) => {
                //debug('local_command received:', command.command, command.args);
                command.origin = 'local';
                this.send_event('command_processor:command', command);
            },
            'debugger_command': (command) => {
                //debug('debugger_command received:', command.command, command.payload);
                this.send_debugger_command(command.command, command.payload);
            },
            'message_received': (message) => {
                this.handle_debugger_message(message);
            },
            'send_debugger_message': (message) => {
                //debug('send_to_debugger received:', message);
                this.send_event('web_socket:send', message)
            },
            'debugger_state_updated': (state) => {
                debug('debugger_state_updated received:', state);
                if (typeof state.source_info == 'object' && state.source_info.location) {
                    let location = state.source_info.location;
                    let filename = location.source;
//                    this.send_event('code_viewer:activate_file', filename);
                    let highlight_lines = [];
                    for (let i = location.start.line; i <= location.end.line; i++) {
                        highlight_lines.push(i);
                    }
                    //debug('file highlights: ' + highlight_lines);
                    this.send_event('code_viewer:activate_file', filename);
                    this.send_event('code_viewer:update_highlights', { filename: filename, highlights: highlight_lines, clear: true });
                }
                this.send_event('watch_area:update_watch_data', state.scope);
                this.send_event('status_bar:update_state', state);
            },
            'debugger_contexts_updated': (contexts) => {
                debug('debugger_contexts_updated received:', contexts);
            },
            'debugger_got_source': (payload) => {
                //debug('debugger_got_source:', payload);
                let file_info = {
                    filename: payload.filename,
                    location: payload.location,
                    text: payload.text,
                    possible_breakpoints: payload.possible_breakpoints,
                }
                this.send_event('code_viewer:update_file', file_info);
            },
            'execution_finished': (payload) => {
                debug('execution_finished received:', payload);
                let context_id = payload.context_id;
                this.send_event('code_viewer:clear_viewer', undefined);
                this.send_event('status_bar:update_state', { paused: false });
            },
            'web_socket:connected': (message) => {
                let state = {
                    connected: true,
                    ws_url: message.url
                };
                this.send_event('status_bar:connection_status', state);
            },
            'web_socket:disconnected': (message) => {
                let state = {
                    connected: false,
                    ws_url: message.url
                };
                this.send_event('status_bar:connection_status', state);
                this.send_event('remote_debugger:debugger_disconnected', message);
            },
            'system:shutdown': () => {
                this.send_event('debugger_command', { command: 'Debugger.resume', payload: {} });
            }
        }
    }
    

    handle_debugger_message(message) {
        //debug('message received from debugger:', message);
        this.send_event('remote_debugger:debugger_message_received', message);
        //this.send_event('log', { origin: 'debugger', message: "message received from debugger: " + util.inspect(message)});
    }

    send_debugger_command(command, payload) {
        this.send_event('remote_debugger:command', { command: command, payload: payload});
    }

    log(message) {
        this.send_event('log', { origin: 'core', message: message });
    }

    add_temporary_listener(event_name, listener) {
	debug('AAAAADDING temp listener for ' + event_name);
        let listeners = this.temp_listeners[event_name];
        if (!Array.isArray(listeners)) {
            this.temp_listeners[event_name] = [];
            listeners = this.temp_listeners[event_name];
        }
        if (!listeners.includes(listener)) {
           listeners.unshift(listener);
        }
    }

    raw_debug(msg) {
        debug(msg);
    }
}

module.exports = Coordinator;
