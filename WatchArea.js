const EventEmitter = require('events');
const blessed = require('blessed');
const NOT_FOUND = Symbol.for('not_found');
const util = require('util');
const ExploreBox = require('./ExploreBox.js');
const DTL = require('dtl-js');
const { debug } = require('./DebugLogger.js');

class WatchArea extends EventEmitter {
    constructor(screen, config) {
        super(); // Call the EventEmitter constructor first

        // Default config
        const defaultConfig = {
            activeStyle: { border: { fg: 'yellow', bold: true } },
            inactiveStyle: { border: { fg: 'white' } },
            // ... more default configurations
            geometry: {
                top: 0,
                left: "80%",
                width: "20%",
                height: "30%",
            }
        };

        // Merge default and user-provided config
        this.config = { ...defaultConfig, ...config };
        this.screen = screen;
        this.things_to_watch = [];
        this.last_data;
        this.sizes = [
            { top: 0, left: '75%', width: '25%', height: 10 }, 
            { width: '100%', height: '100%' }
        ];
        this.explore_box = {
            active: false,
            item: 0,
        };
        this.explore_box.box = new ExploreBox(screen, {});

        this.area = blessed.list({
            parent: screen,
            top: this.config.geometry.top, 
            left: this.config.geometry.left,
            width: this.config.geometry.width,
            height: this.config.geometry.height,
            tags: true,
            interactive: true,
//            draggable: true,
//            mouse: true,
            keys: true,
            border: { type: 'line' },
            style: { selected: { bold: true, } },
            label: ' Watch Area ',
        });
        this.area.setFront();

        this.area.on('select', (item, index) => { 
            if (typeof this.things_to_watch[index] == 'object') {
                //debug('Watch item selected' + index + " " + this.things_to_watch[index].name ); // util.inspect(item)); 
                this.explore_box.active = true;
                this.explore_box.item = index;
                this.explore_box.box.set_data(this.things_to_watch[index].value);
                this.explore_box.box.set_label(this.things_to_watch[index].name);
                this.explore_box.box.show(this.things_to_watch[index].name);
                this.explore_box.box.on('hide', () => {
                    this.explore_box.active = false;
//                    this.area.focus();
                });
            }
        });
        this.area.on('focus', () => {
            this.focus();
        });
        this.area.on('blur', () => {
            this.blur();
        });
        this.on('add_watch_item', (varname) => {
            this.add_watch_item(varname);
        });
        this.on('remove_watch_item', (varname) => {
            this.remove_watch_item(varname);
        });
        this.on('update_watch_data', (data) => {
            this.update_watch_data(data);
        })
    }

    add_watch_item(name) {
        let found = false;
        // make sure we don't add something we are already watching.
        for(let i = 0; i < this.things_to_watch.length; i++) {
           if (this.things_to_watch[i].name == name) {
               found = true;
               break;
           }
        }
        if (found == false) {
            let watch_item = {
                name: name,
                value: NOT_FOUND
            };
            try {
                let result = DTL.apply(this.last_data, '(: ' + watch_item.name + ' :)');
                watch_item.value = result;
                watch_item.content = this.get_watch_display_string(watch_item);
                this.things_to_watch.push(watch_item);
                this.update_list_items(); 
            } catch(e) {
                this.send_event('log', { origin: 'watch_area', message: "unable to parse watch expression: " + name });
            }
        }
    }

    remove_watch_item(name) {
        let found_index;
        for(let i = 0; i < this.things_to_watch.length; i++) {
           if (this.things_to_watch[i].name == name) {
               found_index = i;
               break;
           }
        }
        if (typeof found_index != 'undefined') {
            this.area.remove(this.things_to_watch[found_index]);
            this.things_to_watch.splice(found_index, 1);
            this.update_list_items();
        }
    }

    update_list_items() {
        let item_list = this.things_to_watch.map( (item) => {
            return item.content;
        });
        this.area.setItems(item_list);
    }

    get_watch_display_string(watch_item) {
        let value = watch_item.value;
        let box_width = this.area.width;
        let display_text;
        if (value == NOT_FOUND) {
            value = "N/A"
        }
        if (typeof watch_item.value == 'string') {
            value = '"' + watch_item.value + '"'
        }
        if (typeof watch_item.value == 'object') {
            value = JSON.stringify(watch_item.value);
        }
        display_text = watch_item.name + ": " + value;
        if (display_text.length >= box_width) {
            display_text = display_text.substring(0, (box_width - 4)) + "...";
        }
        return display_text;
    }

    update_watch_data(data) {
        this.last_data = data;
//        debug('watch data updated: '+ util.inspect(data));
        this.things_to_watch.forEach((item, index) => {
            const keys = item.name.split('.');
            let value = DTL.apply(data, { out: "(: " + item.name + ":)" } );
            let different = false;

            /* 
            for (let key of keys) {
                if (value[key] !== undefined) {
                    value = value[key];
                } else {
                    value = NOT_FOUND; // Or some default value when the key path does not exist
                    break;
                }
            }*/
            item.value = value;
            item.content = this.get_watch_display_string(item);
            if (this.explore_box.active) {
                if (this.explore_box.item == index) {
                    if (item.value == NOT_FOUND) {
                        different = true;
                    } else if (typeof value == 'object' && value !== null) {
                        let oldval = JSON.stringify(item.value);
                        let newval = JSON.stringify(value);
                        //debug('watch data: ' + oldval.length + "\n" + newval);
                        
                        /*
                        if (oldval.length != newval.length || oldval != newval) {
                            different = true;
                        }
                        */
                    } else if (item.value != value) {
                        different = true;
                    }
                    if (item.value != NOT_FOUND) {
                        this.explore_box.box.set_data(item.value)
                    }
                }
            }
            this.area.setItem(index, item.content);
        });
        this.area.screen.render();
    }


  // Stub for focus method
    focus() {
        //debug('Watch focused');
        this.area.style.border = this.config.activeStyle.border; 
        this.area.screen.render();
    }

  // Stub for blur method
    blur() {
        //debug('Watch blurred');
        this.area.style.border = this.config.inactiveStyle.border; 
        this.area.screen.render();
    }

  // Stub for update method
    update() {
        this.send_event('log', newData); // Emit an 'update' event with the new data
        // ... your implementation here
    }

    send_event(type, payload) {
        this.emit('message', { type: type, data: payload });
    }

  // ... more methods or static functions as needed
}

module.exports = WatchArea;

