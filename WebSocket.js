const EventEmitter = require('events');
const WebSocket = require('ws');
const uuid = require('uuid');
const util = require('util');
const { debug } = require('./DebugLogger.js');

class DebuggerWebSocket extends EventEmitter {
    constructor(config) {
        super();

        // Initialize WebSocket
        this.ws = false;
        this.wsUrl = 'ws://127.0.0.1:6400';
        this.registrationCode = undefined; 

        this.initialize_internal_handlers();

        // Listen to the 'send' event to send messages
        this.on('send', (message) => {
            //debug('websocket send', message);
            const new_message = this.newMessage(message.type, message.payload);
            this.sendMessage(message);
        });

        // Listen to the 'close_connection' event to close the WebSocket
        this.on('disconnect', () => {
            if (this.ws) {
                this.ws.close();
            }
        });
        this.on('reconnect', () => {
            this.connect(this.wsUrl, this.registrationCode, true);
        });

        this.on('connect', (payload) => {
            debug('websocket connect:', payload);
            if (typeof payload.url == 'string') {
                this.wsUrl = payload.url;
            } 
            if (typeof payload.registration_code == 'string') {
                this.registrationCode = payload.registration_code;
            } 

            this.connect(this.wsUrl, this.registrationCode);
        });
    }

    connect(wsUrl, registrationCode, attempt_resume) {
        if (typeof registrationCode == 'string' && registrationCode.length > 0) {
            this.registrationCode = registrationCode;
        }
        //debug('connect_args:', wsUrl, registrationCode, attempt_resume);
        this.wsUrl = wsUrl;
        let oldws = this.ws;
        // If there's an existing WebSocket connection, close it
        
        if (this.ws && this.ws.readyState === WebSocket.OPEN) {
            delete this.ws;
            oldws.close();
        }

        // Create a new WebSocket connection
        this.ws = new WebSocket(this.wsUrl);

        this.ws.on('open', () => {
            // console.log('Connected to debugger on ' + wsUrl);
            this.sendRegistrationMessage(attempt_resume);
            this.send_event('web_socket:connected', { url: this.wsUrl });
        });

        this.ws.on('message', (message) => this.handleIncomingMessage(message));

        this.ws.on('close', () => {
            // console.log('Connected to debugger on ' + wsUrl);
            this.send_event('web_socket:disconnected', { url: this.wsUrl });
        });

        this.ws.on('error', (e) => {
            debug('websocket error: ', e);
            // console.log('Connected to debugger on ' + wsUrl);
            this.send_event('web_socket:error', { url: this.wsUrl });
            this.ws.close();
        });
        
    }

    newMessage(type, payload, id = uuid.v4()) {
        return {
            id,
            type,
            payload,
            ts: Date.now()
        };
    }

    sendMessage(message) {
        this.ws.send(JSON.stringify(message));
    }

    sendRegistrationMessage(attemptResume) {
        let registration_message = {
            registration_code: this.registrationCode 
        }
        if (attemptResume && this.client_id && this.access_key) {
            registration_message.origin_id = this.client_id;
            registration_message.access_key = this.access_key;
        }
        const message = this.newMessage('client.register', registration_message);
        this.sendMessage(message);
    }

    handleIncomingMessage(message) {
        let parsedMessage;
        try {
            parsedMessage = JSON.parse(message);
            //debug('websocket incoming message', parsedMessage);
        } catch (e) {
            this.send_event('error', `Error parsing message: ${e}`);
            return;
        }

        // Internal message types that this class should handle itself

        let handler = this.handlers[parsedMessage.type];
        if (typeof handler != 'function') {
            handler = this.handlers['relay'];
        } 
        handler(parsedMessage);
    }

    send_event(type, payload) {
        this.emit('message', { type: type, data: payload });
    }


    relayMessageReceived(message) {
        this.send_event('message_received', message);
    }

    // Example internal handler
    handleClientRegistered(data) {
        this.client_id = data.payload.origin_id;
        this.access_key = data.payload.access_key;
        this.send_event('debugger_connected', data.payload); 
    }

    initialize_internal_handlers() {
        this.handlers = {
            'client.registered': this.handleClientRegistered.bind(this),
            'relay': this.relayMessageReceived.bind(this),
        };
    }
}

module.exports = DebuggerWebSocket;
