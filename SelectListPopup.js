const EventEmitter = require('events');
const util = require('util');
const blessed = require('blessed');
const { debug } = require('./DebugLogger.js');

class SelectListPopup extends EventEmitter {
    constructor(screen, config) {
        super(); // Call the EventEmitter constructor first

        // Default config
        const defaultConfig = {
            activeStyle: { border: { fg: 'yellow', bold: true }, selected: { bold: true } },
          inactiveStyle: { border: { fg: 'white', bold: false }, selected: { bold: true }},
          // ... more default configurations
          geometry: {
              top: '30%',
              left: 'center',
              width: '70%',
              height: 10,
          }
        };

        // Merge default and user-provided config
        this.config = { ...defaultConfig, ...config };
        this.screen = screen;
        this.listItems = [];
        this.itemsMap = [];

        this.area = blessed.list({
            parent: screen,
            top: this.config.geometry.top,
            left: this.config.geometry.left,
            width: this.config.geometry.width,
            height: this.config.geometry.height,
            items: this.listItems,
            tags: true,
            interactive: true,
            mouse: true,
            keys: true,
            border: { type: 'line' },
            style: Object.assign({}, this.config.inactiveStyle),
            padding: {
                left: 1,
                right: 1
            },
            label: ' Select Item ',
            hidden: true // Start with the popup hidden
        });

        this.area.on('select', (item, index) => {
            // Emit 'select' event with the item and index
            if (typeof item == 'object') {
                this.emit('select', item.getText(), index);
            }
            this.hide();
        });

        // Hide the popup when escape is pressed
        this.area.key('escape', () => {
            this.emit('escape');
            this.hide();
        });

        this.on('open', (message) => {
            //debug('SelectListPopup open', message);
            this.area.style.border = this.config.activeStyle.border;
            this.area.setLabel(message.label);
            this.onItemSelect(message.on_select);
            this.show(message.items);
        });

        this.on('close', () => {
            this.area.style.border = this.config.inactiveStyle.border;
            this.hide();
        });
    }

    // Show the popup with a list of items
    show(items) {
        // Reset the internal map
        debug('SELECTING:::' + util.inspect(items));
        this.itemsMap = items;

        // Extract the text values to display in the list
        const displayItems = items.map(item => item.text);
        this.area.setItems(displayItems); // Set the items to list
        this.area.focus(); // Focus on the list for keyboard input
        this.area.style.border = this.config.activeStyle.border; // Highlight the popup
        this.area.show(); // Make the popup visible
        this.screen.render(); // Re-render the screen to display changes
    }

    // Hide the popup
    hide() {
        this.area.hide(); // Hide the popup
        this.area.style.border = this.config.inactiveStyle.border; // Remove highlight
        this.screen.render(); // Re-render the screen to remove the popup from view
    }

    onItemSelect(callback) {
        this.area.on('select', (item, index) => {
            // Use the index to get the id from the itemsMap
            const result_item = this.itemsMap[index];
            callback(result_item);
        });
    }
}

module.exports = SelectListPopup;
