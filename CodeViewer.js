const blessed = require('blessed');
const util = require('util');
const { EventEmitter } = require('events');
const { debug } = require('./DebugLogger.js');

class CodeViewer extends EventEmitter {
    constructor(screen, config) {
        super();

        const defaultConfig = {
            activeStyle: { track: { bg: "yellow", bold: true }, border: { fg: 'yellow', bold: true } },
            inactiveStyle: { border: { fg: 'white' }, track: { bg: "#222222", bold: false} },
            geometry: {
                top: 0,
                left: 0,
                width: "100%",
                height: "70%",
            },
            highlight: {
                start: "{bold}{green-fg}",
                end: "{/green-fg}{/bold}",
            },
            line_numbers: {
                start: "{grey-fg}",
                end: "{/grey-fg}"
            }
            // ... more default configurations
        };

        // Merge default and user-provided config
        this.config = { ...defaultConfig, ...config };

        this.screen = screen;
        this.files = {};
        this.active_file;

        // Create the box with initial settings
        this.area = blessed.box({
            parent: this.screen,
            top: this.config.geometry.top, 
            left: this.config.geometry.left,
            width: this.config.geometry.width,
            height: this.config.geometry.height,
            keys: true,
            tags: true,
//            mouse: true,
            scrollable: true,
            alwaysScroll: true,
            scrollbar: {
                ch: '#',
                track: {
                    bg: '#222222',
                },
            },
            style: {
                fg: "cyan",
                bold: false,
                focus: {
                    border: {
                        fg: 'yellow',
                        bold: true
                    }
                }
            },
            border: 'line',
            label: ' Code '
        });
        this.area.get_scroll = function() {
            return this.childBase;
        }.bind(this.area);

        // we don't close the file viewer
        // this.area.key(['escape'], () => this.hide());
        this.area.on('focus', () => { 
            this.area.style.track = this.config.activeStyle.track;
            //debug('CODEVIEW focus');
        })
        this.area.on('blur', () => { 
            this.area.style.track = this.config.inactiveStyle.track;
            //debug('CODEVIEW BLUR');
        })
        this.on('update_file', (file_info) => {
            this.update_file(file_info);
        });
        this.on('update_highlights', (message) => {
            //debug('UPDATE_HIGHLIGHTS!' + util.inspect(message));
            this.update_highlights(message.filename, message.highlights, true);
        });
        this.on('clear_viewer', () => {
            this.active_file = undefined;
            this.area.setContent(' ');
            this.area.setLabel({ text: ' No File Loaded ', side: "left" });
            this.screen.render();
        });
        this.on('activate_file', (filename) => {
            if (this.active_file != filename) {
                this.switch_active_file(filename);
            }
        });
    }

    
    is_line_visible(line_num) {
        let first_line = this.area.childBase;
        let last_line = first_line + this.area.height;
        if (line_num >= first_line && line_num < last_line ) {
            return true;
        } else {
            return false;
        }
    }
    

    scroll_to_visible(lineNumber) {
        let targetLine = lineNumber + 3;

        // don't scroll if the line is already visible
        if (this.is_line_visible(targetLine)) {
            return;
        }
        // Get the height of the box to determine how many lines can be displayed at once.
        const boxHeight = this.area.height - 2;

        // Calculate the new scroll position.
        // We subtract 3 from the boxHeight to ensure that the next three lines are visible.
        let newPosition = targetLine - boxHeight;


        // Prevent the new position from going below 0.
        newPosition = Math.max(newPosition, 0);

        this.area.scrollTo(newPosition);

        // Rerender the screen
        this.area.screen.render();
    }

    update_file(file_info) {
        let data = this.get_cached_file(file_info.filename);
        data.filename = file_info.filename;
        data.location = file_info.location;
        if (file_info.location && file_info.location.start) {
            data.start_offset = file_info.location.start.line;
        }
        data.text = file_info.text;
        data.possible_breakpoints = file_info.possible_breakpoints;
        if (typeof data.highlights != 'object') {
            data.highlights = {};
        }
        data.lines = this.get_code_lines(data.text, data.start_offset);
        this.files[file_info.filename] = data;
        //debug('UPDATE_FILE' + util.inspect(data));
        if (this.active_file == data.filename) {
            this.update_display();
        }
    }

    switch_active_file(filename) {
        let data = this.get_cached_file(filename);
        this.send_get_source_request(filename);
        this.active_file = filename;
        if (typeof data != 'object') {
            data = {
                filename: filename,
                location: {},
                text: "no file data available",
                data_requested: true,
                possible_breakpoints: []
            };
            this.update_file(data);
        }
        this.update_display();
    }
    
    get_code_lines(data, offset) {
        let lines = data;
        let new_lines = [];
        if (typeof lines == 'string') {
            lines = data.split(/\n/g);
        }
        if (typeof offset == 'number') {
            // our code starts some number of lines in.
            for( let i = 0; i < offset; i++) {
                new_lines.push('˙'.repeat(76)); //String.fromCharCode(176).repeat(76));
            }
        }
        lines.forEach((line) => {
            new_lines.push(line);
        });

        return new_lines;
    }

    get_cached_file(filename) {
        let data = this.files[filename];
        if (typeof data == 'undefined') {
            data = {
                filename: filename,
                location: {},
                text: "no file data available",
                lines: [],
                data_requested: true,
                possible_breakpoints: [],
                highlights: []
            };
            this.files[filename] = data;
        }
        return data;
        
    }

    update_highlights(filename, highlights, clear) {
        let data = this.get_cached_file(filename);
        if (clear) {
            data.highlights = {};
        }

        if (Array.isArray(highlights)) {
            highlights.forEach( (line) => {
                data.highlights[line] = true;
            });
        } else if (typeof highlights == 'object') {
            Object.keys(highlights).forEach( (key) => {
                data.highlights[key] = highlights[key];
            });
        }
        //debug('HIGHLIGHTS: ' + util.inspect(data.highlights));
        if (this.active_file == data.filename) {
            this.update_display();
        }
    }

    update_display() {
        let new_content = [];
        let line_content, line_prefix;
        let line;
        let last_highlight = 0;
        let active_file = this.get_cached_file(this.active_file);
        let line_number_width = 1 + active_file.lines.length.toString().length;
        let padding = ' '.repeat(line_number_width);
        if (this.area.get_scroll() < active_file.start_offset) {
            this.area.scrollTo(active_file.start_offset);
        }

        //debug('ACTIVE' + util.inspect(active_file));
        for (let i = 0; i < active_file.lines.length; i++) {
            line_prefix = this.config.line_numbers.start + (padding + i).slice(0 - line_number_width) + this.config.line_numbers.end;
            line = active_file.lines[i];
            let highlight = active_file.highlights[i]; 
            if (i < active_file.start_offset ) {
                line_content = "{grey-fg}" + blessed.escape(line) + "{/grey-fg}";
            } else if (typeof highlight != 'undefined') {
                if (typeof highlight == 'object') {
                    line_content = highlight.start + blessed.escape(line) + highlight.end;
                } else if (typeof highlight == 'string') {
                    line_content = "{" + highlight + "}" + blessed.escape(line) + "{/" + highlight + "}";
                } else if (highlight == true) {
                    line_content = this.config.highlight.start + blessed.escape(line) + this.config.highlight.end;
                } else {
                    line_content = blessed.escape(line);
                }
                last_highlight = i;
            } else {
                // no highlight
                line_content = blessed.escape(line);
            }
            new_content.push(line_prefix + " " + line_content);
        }
        this.area.setLabel({ text: ' {bold}' + active_file.filename + '{/bold} ', side: "left" });
        this.area.setContent(new_content.join('\n'));
        this.scroll_to_visible(last_highlight);
        this.screen.render();
    }

    send_get_source_request(filename) {
//        debug('ASKING FOR SOURCE:', filename);
        this.send_event('debugger_command', { command: 'Debugger.get_source', payload: { filename: filename } });
    }

    send_event(type, payload) {
        this.emit('message', { type: type, data: payload });
    }

}

module.exports = CodeViewer;
