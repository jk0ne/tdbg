const blessed = require('blessed');
const EventEmitter = require('events');
const { debug } = require('./DebugLogger.js');

class StatusBar extends EventEmitter {
    constructor(screen, config) {
        super();

        this.screen = screen;
        this.session_name = config.session_name;
        this.defaults = {
            paused: 'Unknown',
            active_file: '',
            line_number: '',
            line_text: '',
            is_connected: false
        };

        this.state = {
            paused: 'Unknown',
            active_file: '',
            line_number: '',
            line_text: '',
            is_connected: false
        };

        this.area = blessed.box({
            parent: screen,
            bottom: 0,
            left: 0,
            width: '100%',
            height: 1,
            tags: true,
            style: {
                fg: 'white',
                bold: true,
                bg: 'green',
                border: {
                    fg: '#f0f0f0'
                }
            }
        });

        this.on('update_state', this.updateFromDebugger.bind(this));
        this.on('connection_status', (data) => {
            this.updateState({ is_connected: data.connected })
        });
        this.updateState({});
        debug("Hi, My name is: " + this.session_name);
    }

    attachToScreen(screen) {
        screen.append(this.area);
    }

    updateFromDebugger( data ) {
        //debug('YOOOOOOOOOO status_bar ', data);
        let state = {};
        state.paused = data.paused ? 'Paused' : 'Running'
        if (typeof data.source_info == 'object' && typeof data.source_info.location == 'object' ) {
            state.active_file = data.source_info.location.source || data.source_info.name,
            state.line_number = data.source_info.location.start.line
            if (typeof data.source_info.text == 'string') {
                state.line_text = data.source_info.text.substring(0, 20) + "...";
            } else {
                state.line_text = '';
            }
        } else {
            state.active_file = '';
            state.line_number = '';
            state.line_text = ''
        }

        this.updateState(state);
    }

    updateState(new_state) {
        const fields = [ "paused", "active_file", "line_number", "line_text", "is_connected"]

        fields.forEach( (field) => {
            if (typeof new_state[field] != 'undefined') {
                this.state[field] = new_state[field];
            } else if (typeof this.state == 'undefined') {
                this.state[field] = this.defaults[field];
            }
        });
        //debug('BBAAAAAAAAAAARRRRRRRRR', this.state);

        this.refreshDisplay();
    }

    refreshDisplay() {
        //debug('FOOOOOOOOOOOOOOOOOOOOOOOO');
        const { paused, active_file, line_number, line_text, is_connected } = this.state;
        const connectionStatus = is_connected ? '{green-bg} C {/}' : '{red-bg} D {/}';
        const fileLine = active_file && line_number ? `${active_file}:${line_number}` : '';
        let session_name = ''
        if (typeof this.session_name == 'string') {
            session_name = this.session_name + " | ";
        }
        const statusText = `${connectionStatus}| ${session_name}${paused} | ${fileLine} {|}${line_text}`;
        this.area.setContent(statusText);
        this.area.screen.render();
    }

    send_event(type, payload) {
        this.emit('message', { type: type, data: payload });
    }
}

module.exports = StatusBar;

