#!/usr/bin/env node 
const util = require('util');
const uuid = require('uuid');
const fs = require('fs');
const path = require('path');
const blessed = require('blessed');
const { program } = require('commander');
const { debug } = require('./DebugLogger.js');
const Coordinator = require('./Coordinator.js');
const WebSocket = require('./WebSocket.js');
const DisplayArea = require('./DisplayArea.js');
const WatchArea = require('./WatchArea.js');
//const InputArea = require('./InputArea.js');
const CommandProcessor = require('./CommandProcessor.js');
const RemoteDebuggerHandler = require('./RemoteDebuggerHandler.js');
const StatusBar = require('./StatusBar.js');
const ExploreBox = require('./ExploreBox.js');
const CodeViewer = require('./CodeViewer.js');
const CommandBox = require('./CommandBox.js');
const SelectListPopup = require('./SelectListPopup.js');


// Command line options with commander
program.option('-r, --registration_code <type>', 'Registration code to connect to the debugger server')
  .option('-c, --config <config_file>', 'Config file to load')
  .option('-n, --name <session_name>', 'Name for this session')
  .option('-l, --log_activity', 'Log all DAP messages to the debug log')
  .argument('[wsurl]', 'WebSocket URL', 'ws://127.0.0.1:6400') // Provide default URL
  .parse();

let command_history_file = process.env.HOME + "/.tdbg_history";
let command_history_length = 5000;
if (process.env.TDBG_HISTFILE) {
    command_history_file = process.env.TDBG_HISTFILE;
}
if (process.env.TDBG_HISTSIZE) {
    command_history_length = process.env.TDBG_HISTSIZE;
}


let args = program.opts();
let wsUrl = program.args[0]; // This will have the WebSocket URL or the default one
let config = {
    input_area: {},
    display_area: {},
    watch_area: {},
    web_socket: {},
    command_processor: {},
    remote_debugger: {
        log_activity: args.log_activity
    },
    coordinator: {},
    status_bar: {
        session_name: args.name
    },
    code_viewer: {},
    select_list: {}
};

if (!wsUrl) {
    wsUrl = 'ws://127.0.0.1:6400'; // Default WebSocket URL
}

// Hotkeys object
const hotkeys = {
    'C-c': {
        fn: () => {
            coordinator.send_event('system:shutdown_requested');
        },
        help: 'quit tdbg'
    },
    'S-up': {
        fn: () => {
            display_area.scroll(-1);
            screen.render();
        },
        help: 'Scrolls display area up by one line.'
    },
    'S-down': {
        fn: () => {
            display_area.scroll(1);
            screen.render();
        },
        help: 'Scrolls display area down by one line.'
    },
    'S-pageup': {
        fn: () => {
            display_area.scroll_page(-1);
            screen.render();
        },
        help: 'Scrolls display area up by one page.'
    },
    'S-pagedown': {
        fn: () => {
            display_area.scroll_page(1);
            screen.render();
        },
        help: 'Scrolls display area down by one page.'
    },
    'S-end': {
        fn: () => {
            display_area.scroll_end();
            screen.render();
        },
        help: 'Scrolls display area down by one page.'
    },
    'tab': {
        fn: switch_focus_area,
        help: 'Toggle between active areas.',
    },
    'A-t': {
        fn: resizeActiveArea,
        help: 'Resize active area.',
    },
    'f1': {
        fn: () => {
            coordinator.send_event('command_processor:command', { command: 'help', args: [] });
        },
        help: 'Show help'
    }
    // Add more hotkeys here
};

let hotkey_list = Object.keys(hotkeys);

const screen = blessed.screen({
    smartCSR: true,
    ignoreLocked: hotkey_list,
    debug: true
});

// Initialize your objects
const input_area = new CommandBox(screen, config.input_area);
//const input_area = new InputArea(screen, config.input_area);
const display_area = new DisplayArea(screen, config.display_area);
const watch_area = new WatchArea(screen, config.watch_area);
const ws = new WebSocket(config.websocket);
const command_processor = new CommandProcessor(config.command_processor);
const remote_debugger = new RemoteDebuggerHandler(config.remote_debugger);
const status_bar = new StatusBar(screen, config.status_bar);
const explore_box = new ExploreBox(screen, config.explore_box);
const code_viewer = new CodeViewer(screen, config.explore_box);
const select_list = new SelectListPopup(screen, config.select_list);

// Create Coordinator and register modules
const coordinator = new Coordinator(screen, config.coordinator);
coordinator.register_module('display_area', display_area);
coordinator.set_log_module('display_area');
coordinator.register_module('input_area', input_area);
coordinator.register_module('watch_area', watch_area);
coordinator.register_module('web_socket', ws);
coordinator.register_module('command_processor', command_processor);
coordinator.register_module('remote_debugger', remote_debugger);
coordinator.register_module('status_bar', status_bar);
coordinator.register_module('explore_box', explore_box);
coordinator.register_module('code_viewer', code_viewer);
coordinator.register_module('select_list', select_list);

screen.append(display_area.area);
screen.append(input_area.area);
screen.append(watch_area.area);
screen.append(status_bar.area);
//screen.append(explore_box.area);
//
coordinator.send_event('web_socket:connect', { url: wsUrl, registration_code: args.registration_code });

const quitPrompt = blessed.question({
    parent: screen,
    top: 'center',
    left: 'center',
    width: 'shrink',
    height: 'shrink',
    border: 'line',
    style: { 
        border: { fg: 'red', bold: true },
        fg: 'white',
        bold: true
    },
    label: ' Quit ',
});
screen.append(quitPrompt);

// Active areas and their default settings
const areas = [
    input_area, 
    watch_area,
    code_viewer,
    display_area,
];
let active_area = 0;


function showQuitPrompt() {
    screen.saveFocus();
    quitPrompt.setFront();
    //quitPrompt.show();
    quitPrompt.ask('Quit: Are you sure? (y/n)', (err, value) => {
        if (value) {
            coordinator.send_event('system:shutdown');
            shutdown();
        }
        //areas[old_area].area.focus();
        screen.restoreFocus();
        screen.render(); // To remove the prompt
    });
    quitPrompt.focus();
    screen.render();
}

coordinator.on('system:shutdown_requested', () => {
    debug('SHUTDOWN_REQUESTED')
    showQuitPrompt();
});

coordinator.on('system:shutdown', () => {
    shutdown();
});

let my_connection = {};
let currentActiveIndex = 0; // start with inputArea
let ticktock = "tick";
load_history();

function load_history() {
    if (typeof command_history_file == 'string' && command_history_file.length > 0) {
    // Resolve the full path for the filename
        const fullPath = path.resolve(command_history_file);

        fs.readFile(fullPath, { encoding: 'utf-8' }, (err, data) => {
            if (!err) {
                // Split the file content by new lines and log the array
                const lines = data.split(/\r?\n/); // This regex handles both LF and CRLF
                input_area.setCommandHistory(lines);
            }
        });
    }
}

function save_history() {
    try {
        if (typeof command_history_file == 'string' && command_history_file.length > 0) {
            let commands = input_area.getCommandHistory();
            let hist_size = command_history_length || 5000;
            let lines = commands.slice(Math.max(commands.length - hist_size, 0));
            if (lines.length > 0) {
                fs.writeFileSync(command_history_file, lines.join('\n'), { flag: 'w' });
            }
        }
    } catch(e) {
        console.error('Unable to save history file: ', e);
    }
}



function shutdown(reason) {
    // shut down app.
    coordinator.send_event('log', { origin: 'system', message: 'Shutting Down...' + ( reason || '') });
    save_history();
    setTimeout(() => { process.exit() }, 100);
}

function switch_focus_area() {

    let focused = screen.focused;
    let allow_switch = false;
    areas.forEach( (box) => {
        if (box.area == focused) {
            allow_switch = true;
        }
    });
    if (allow_switch) {
        active_area = (active_area + 1) % areas.length;

        // Focus the next area
        areas[active_area].area.focus();

        // Render screen for the focus change to take effect
        screen.render();
    }
}

function blurAll() {
    areas.forEach( (area) => {
        if (typeof area.area.blur == 'function') {
            area.area.blur();
        }
    });
}

// Function to resize active area
function resizeActiveArea() {
    let activeArea = areas[currentActiveIndex];
    activeArea.currentIndex = (activeArea.currentIndex + 1) % activeArea.sizes.length;
    let newSize = activeArea.sizes[activeArea.currentIndex];

    Object.assign(activeArea.box.options, newSize);
    activeArea.box.emit('attach'); // re-render
    screen.render();
}

input_area.on('command', function (event) {
    if (commands[event.command]) {
        commands[event.command].fn(event.args);
    } else {
        coordinator.send_event('log', { origin: 'user', message: `Unknown command: ${event.command}`});
    }
});


// Add hotkey listener
screen.key(Object.keys(hotkeys), function (ch, key) {
    //coordinator.send_event('debug', 'hotkey pressed:'+ util.inspect(key));
    const hotkey = key.full;
    if (hotkeys[hotkey]) {
        hotkeys[hotkey].fn();
    }
});

screen.render();
input_area.area.focus();
debug('tdbg activated');
